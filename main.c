#include <stdio.h>

//Funcion utilizda para generar potencias
int intPow(int iArg, int iPower);

//Funcion que convierte de binario a entero
int asciiBinaryToInt(char *s);

//Funcion que convierte de hexadecimal a entero
int asciiHEXToInt(char *s);

//Funcion que convierte de ascii a decimal
double asciiToDouble(char *s);

//Funcion Principal
int main(int argc, char *argv[])
{
    int iOpcion;
    char cArrResp[50];
    
    //MENU para llamar funciones
    do{
        printf("CONVERSIONES:\n");
        printf("1. Binario a Entero:\n");
        printf("2. Hexadecimal a Entero:\n");
        printf("3. ASCII a Decimal\n");
        printf("0. Salir\n");
        printf("Seleccionar Opcion: ");
        
        scanf("%d", &iOpcion);
        
        switch(iOpcion)
        {
            case 1:
                printf("Ingresa numero binario: ");
                scanf("%s", cArrResp);
                printf("Numero entero: %d\n", asciiBinaryToInt(cArrResp));
                break;
            case 2:
                printf("Ingresa numero Hexadecimal: ");
                scanf("%s", cArrResp);
                printf("Numero entero: %d\n", asciiHEXToInt(cArrResp));
                break;
            case 3:
                printf("Ingresa secuencia de caracteres numericos: ");
                scanf("%s", cArrResp);
                printf("Numero decimal: %lf\n", asciiToDouble(cArrResp));
                break;
            case 0:
                printf("Gracias!\n");
                break;
            default:
                printf("Opcion invalida\n");
        }
        
    }while(iOpcion != 0);
}