#include <string.h>
#include <math.h>
#include <stdlib.h>

int intPow(int iArg, int iPower)
{
    int iNum = 1;
    
    for(int i = 0; i < iPower; i++)
    {
        iNum *= iArg;
    }
    
    return iNum;
}

int asciiBinaryToInt(char *s)
{
    char *cpSearch;
    int iSum = 0;
    int iValChar = 1;
    
    //Evalua los terminos binarios en decimal
    if(strlen(s) <= 32)
    {
        for(cpSearch = s; *cpSearch != '\0' && iValChar; cpSearch++)
        {
            switch(*cpSearch)
            {
                case '1': iSum += intPow(2, cpSearch-s);
                break;
                case '0':
                break;
                default: iValChar = 0;
            }
        }
    }
    return iSum;
}

int asciiHEXToInt(char *s)
{
    char *cpSearch;
    int iSum = 0;
    int iValChar = 1;
    
    //Evalua los terminos hexadecimales a decimal
    if(strlen(s) <= 8)
    {
        for(cpSearch = s; *cpSearch != '\0' && iValChar; cpSearch++)
        {
            if(*cpSearch >= '1' && *cpSearch <= '9')
            {
                iSum += (*cpSearch - 48) * intPow(16,cpSearch-s);
            }
            else if(*cpSearch >= 'A' && *cpSearch <= 'F')
            {
                iSum += (*cpSearch - 55) * intPow(16,cpSearch-s);
            }
            else if(*cpSearch >= 'a' && *cpSearch <= 'f')
            {
                iSum += (*cpSearch - 87) * intPow(16,cpSearch-s);
            }
            else
            {
                iValChar = 0;
            }
        }
    }
    return iSum;
}

double asciiToDouble(char *s)
{
    char *cpSearch;
    char cArrAux[10] = "";
    double dSum = 1.0;
    int iDecimal = 0, iSign = 0, iPow = 1;
    
    //Verifica los primeros numeros antes de un punto decimal
    for(cpSearch = s; *cpSearch != '\0' && !iDecimal; cpSearch++)
    {
        if(*cpSearch == '-')
        {
            iSign = 1;
        }
        else if(*cpSearch >= '0' && *cpSearch <= '9')
        {
            cArrAux[cpSearch-s] = *cpSearch;
        }
        else if(*cpSearch == '.')
        {
            iDecimal = 1;
        }
    }
    
    dSum = (double) atoi(cArrAux);
    
    //Condicion para saber si hubo signo negativo
    if(iSign)
        dSum *= -1.0;
    
    //Ciclo para generar numeros decimales
    while(cpSearch != 0)
    {
        if(*cpSearch >= '0' && *cpSearch <= '9')
        {
            dSum += (*cpSearch - 48.0)  / (double) intPow(10,iPow);
            iPow++;
        }
        else
            break;
        cpSearch++;
    }
    
    return dSum;
}
